package org.payloads;

import org.Entities.Scene;

import java.util.List;

/**
 *
 *    "scène": string,
 *    "id":int,
 *    "participants":[
 *      { [key:string]: {
 *          "qualificateurs":[ { [key:string] : string} ],
 *          "roles":string
 *      }}
 *    ],
 *    "phrases":[
 *      { [key:string]:{
 *          "texte":string,
 *          "émotion":"colère/peur/tristesse/joie",
 *          "raisons":[
 *            {
 *              [key:string]:{ "debut": int, "fin": int }
 *            }
 *          ]
 *      }}
 *    ],
 *    "error":null
 *  }
 */

public class ScenePOJO {
  String text;
  int id;
  List<Scene.Participant> participants;
  List<Scene.Phrase> phrases;
  String error;

  public ScenePOJO(String text, int id, List<Scene.Participant> participants, List<Scene.Phrase> phrases, String error) {
    this.text = text;
    this.id = id;
    this.participants = participants;
    this.phrases = phrases;
    this.error = error;
  }

  public String getText() {
    return text;
  }

  public int getId() {
    return id;
  }

  public List<Scene.Participant> getParticipants() {
    return participants;
  }

  public List<Scene.Phrase> getPhrases() {
    return phrases;
  }
}
