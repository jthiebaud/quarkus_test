package org.payloads;

import org.Entities.Scene;

import java.util.List;

/**
 * {
 *  "scènes":[
 *    { "id":int,
 *      [key:string] : string,
 *      "participants":[
 *        { [key:string]: {
 *            "qualificateurs":[ { [key:string] : string } ],
 *            "rôles":"narrateur"
 *        }}
 *      ],
 *      "phrases":[
 *        { [key:string] :{
 *            "texte":string,
 *            "émotion":"colère/peur/tristesse/joie",
 *            "raisons":[
 *              { [key:string] : { "debut":int, "fin":int } }
 *            ]
 *        }}
 *      ]
 *    }
 *  ]
 * }
*/

public class SearchResultsPOJO {
  List<Scene> scenes;
}
