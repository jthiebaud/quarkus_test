package org.payloads;

public class AnnotationResponsePOJO {
  private final boolean result;
  private final String message;
  private final int code;

  public AnnotationResponsePOJO(boolean result, String message, int code) {
    this.result = result;
    this.message = message;
    this.code = code;
  }

  public boolean isResult() {
    return result;
  }

  public String getMessage() {
    return message;
  }

  public int getCode() {
    return code;
  }
}
