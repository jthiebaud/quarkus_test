package org.acme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.Entities.Scene;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.payloads.ScenePOJO;

import java.util.ArrayList;
import java.util.List;

@Path("/API")
public class SceneResource {
    @GET
    @Path("/getScene/{id}")
    public ScenePOJO getSceneById(@PathParam String id) {
        // bdd.getSceneById(id)
        var participants = List.of(
            new Scene.Participant(List.of(List.of("Je", "Moi")), "Narrateur")
        );

        var phrases = List.of(
            new Scene.Phrase("c'est la phrase", Scene.Emotion.Joie, List.of(List.of(0, 15)))
        );

        return new ScenePOJO(
            "c'est la scène",
            1,
            participants,
            phrases,
            "null"
        );
    }

    @GET
    @Path("/search/{text}")
    public ScenePOJO getSceneByText(@PathParam String text) {
        // bdd.searchSceneByText(id)
        return null /*new SearchResultsPOJO()*/;
    }
}