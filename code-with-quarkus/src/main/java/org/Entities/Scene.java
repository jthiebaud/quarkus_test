package org.Entities;

import java.util.List;

public class Scene {
  int id;
  String text;
  List<Participant> participants;
  List<Phrase> phrases;

  public Scene(int id, String text, List<Participant> participants, List<Phrase> phrases) {
    this.id = id;
    this.text = text;
    this.participants = participants;
    this.phrases = phrases;
  }

  public enum Emotion {
    Colere,
    Peur,
    Tristesse,
    Joie
  }

  public static class Participant {
    List<List<String>> qualificateurs;
    String role;

    public Participant(List<List<String>> qualificateurs, String role) {
      this.qualificateurs = qualificateurs;
      this.role = role;
    }
  }

  public static class Phrase {
    String text;
    Emotion emotion;
    List<List<Integer>> raisons;

    public Phrase(String text, Emotion emotion, List<List<Integer>> raisons) {
      this.text = text;
      this.emotion = emotion;
      this.raisons = raisons;
    }
  }
}
